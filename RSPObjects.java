
public class RSPObjects {
	
	private int win=0, lose=0, tie = 0; // game state
	
	public static enum gameState{
		WIN, LOSE, TIE;
	}
	
	//return wins 
	public int getWin(){
		return win;
	}
	//return lose
	public int getLose(){
		return lose;
	}
	//return tie
	public int getTie(){
		return tie;
	}
	//restart new game
	public void setNewGame(){
		win = 0;
		lose = 0;
		tie =0;
	}
	
	//Game state count
	public gameState stateCount(String input, String random){
		if(input.equalsIgnoreCase("ROCK")){
			
			if(input.equalsIgnoreCase("PAPER")){
				lose++;
				return gameState.LOSE;
			}else if(random.equalsIgnoreCase("SCISSOR")){
				win++;
				return gameState.WIN;
			}
		}else if(input.equalsIgnoreCase("PAPER")){
			if(random.equalsIgnoreCase("SCISSOR")){
				lose++;
				return gameState.LOSE;
			}else if(random.equalsIgnoreCase("ROCK")){
				win++;
				return gameState.WIN;
			}
		}else if(input.equalsIgnoreCase("SCISSOR")){
			if(random.equalsIgnoreCase("PAPER")){
				win++;
				return gameState.WIN;
			}else if(random.equalsIgnoreCase("ROCK")){
				lose++;
				return gameState.LOSE;
			}
		}
		// if non of all above occur count as a tie
		tie++;
		return gameState.TIE; //return game state as tie + 1 
	}
	
	public String getRandomChoice(){
        double rand = Math.random();

        if(rand < .33){
            return "ROCK";
        }
        else if(rand < .66){
            return "PAPER";
        }
        else{
            return "SCISSOR";
        }
    }
	
}
