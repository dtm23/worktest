import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;


public class ObjectListener implements ActionListener {

	JLabel player, computer, result, label_win,label_lose,label_tie;
	JButton button_rock,button_paper,button_scissor;
	
	RSPObjects object;

	public ObjectListener(JLabel player,JLabel computer, 
			JLabel label_win, JLabel label_lose, JLabel label_tie,
			JButton button_rock,JButton button_paper, JButton button_scissor,
			JLabel result){
		this.player = player;
		this.computer = computer;
		this.label_win = label_win;
		this.label_lose = label_lose;
		this.label_tie = label_tie;
		this.button_rock = button_rock;
		this.button_paper = button_paper;
		this.button_scissor = button_scissor;
		this.result = result;
		this.object = new RSPObjects();
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		String playerChoice = "UNKNOWN"; 


		if(event.getSource() == button_rock){
			playerChoice = "ROCK";
		}else if(event.getSource() == button_paper){
			playerChoice = "PAPER";
		}else if(event.getSource() == button_scissor){
			playerChoice = "SCISSOR";
		}
		String computerChoice = object.getRandomChoice();

		player.setText("Player Chose - " + playerChoice );
		computer.setText("Computer Chose - " + computerChoice);
		
		RSPObjects.gameState state = object.stateCount(playerChoice, computerChoice);
		
		if(state == RSPObjects.gameState.WIN){
			result.setText("Player's Win");
		}else if(state == RSPObjects.gameState.LOSE){
			result.setText("Computer's Win");
		}else{
			result.setText("Tie");
		}
		

		label_win.setText("Wins: " + object.getWin());
		label_lose.setText("Lose: " + object.getLose());
		label_tie.setText("Tie: " + object.getTie());
	}

}
