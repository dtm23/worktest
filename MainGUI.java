import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;


public class MainGUI {

	static JLabel player, computer, result, label_win,label_lose,label_tie;
	static JButton button_rock,button_paper,button_scissor;
	private static JLabel lblChoseYoursNext;
	private static JMenuItem mntmExit;
	static RSPObjects object;
	public static void main(String args[]){
		
		JFrame frame = new JFrame("GAME ==( ROCK-SCISSOR-PAPER )==");
		
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		player = new JLabel("", JLabel.CENTER);
		computer = new JLabel("", JLabel.CENTER);
		result = new JLabel("GAME RESULTS",JLabel.CENTER);
		 
		button_rock = new JButton("ROCK");
		button_rock.setFont(new Font("Tahoma", Font.BOLD, 11));
		button_rock.setBounds(30, 44, 106, 39);
		button_paper= new JButton("PAPER");
		button_paper.setFont(new Font("Tahoma", Font.BOLD, 11));
		button_paper.setBounds(164, 44, 109, 39);
		button_scissor = new JButton("SCISSOR");
		button_scissor.setFont(new Font("Tahoma", Font.BOLD, 11));
		button_scissor.setBounds(297, 44, 115, 39);
		 
		label_win = new JLabel("Win  = 0", JLabel.CENTER);
		label_lose = new JLabel("Lose  = 0", JLabel.CENTER);
		label_tie = new JLabel("tie = 0", JLabel.CENTER);
		 
		ActionListener actionListener = new ObjectListener( player, computer, 
					 label_win,  label_lose,  label_tie,
					 button_rock, button_paper,  button_scissor, result);
		 
		 button_rock.addActionListener(actionListener);
		 button_paper.addActionListener(actionListener);		 
		 button_scissor.addActionListener(actionListener);
		 
		 
		 /// add Panel top
		 JPanel panel = new JPanel();
		 panel.setLayout(new BorderLayout());
		 
		 panel.add(player, BorderLayout.WEST);
		 panel.add(computer,BorderLayout.EAST);
		 panel.add(result,BorderLayout.SOUTH);
		 
		 // add panel middle
		 JPanel panel2 = new JPanel();
		 panel2.setBackground(SystemColor.activeCaption);
		 panel2.setLayout(null);
		 panel2.add(button_rock);
		 panel2.add(button_paper);
		 panel2.add(button_scissor); 
		 
		 
		 //add panel bottom
		 JPanel panel3 = new JPanel();
		 panel3.setLayout(new FlowLayout());
		 
		 panel3.add(label_win);
		 panel3.add(label_lose);
		 panel3.add(label_tie);

		 frame.getContentPane().setLayout(new BorderLayout());
		 
		 frame.getContentPane().add(panel,BorderLayout.SOUTH);
		 frame.getContentPane().add(panel2,BorderLayout.CENTER);
		 frame.getContentPane().add(panel3,BorderLayout.NORTH);
		 
		 lblChoseYoursNext = new JLabel("CHOSE YOUR'S NEXT MOVE");
		 lblChoseYoursNext.setForeground(SystemColor.desktop);
		 lblChoseYoursNext.setBounds(30, 13, 382, 20);
		 panel2.add(lblChoseYoursNext);
		 
		 
		 frame.setSize(450,236);
		 
		 JMenuBar menuBar = new JMenuBar();
		 frame.setJMenuBar(menuBar);
		 
		 JMenu mnFile = new JMenu("File");
		 menuBar.add(mnFile);

		 
		 mntmExit = new JMenuItem("Exit");
		 mntmExit.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 		System.exit(0);
		 	}
		 });
		 mnFile.add(mntmExit);
		 frame.setVisible(true);
	}
}
